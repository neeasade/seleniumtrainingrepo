package com.xpanxion.training.java.selenium.core;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import com.xpanxion.training.java.selenium.core.utils.WaitUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>Base PageObject class.
 * 
 * <p>This class is the intended root class for all Selenium Page Objects.  All page objects should
 * extend this class, and provide the Selenium WebDriver object as an argument constructor to
 * interact with the current web page.
 * 
 *
 */
public class PageObject {

	protected WebDriver driver;
	
	/**
	 * Instantiates this PageObject and subsequently loads all of the WebElements associated
	 * with this PageObject, annotated with @FindBy and other Selenium PageFactory annotations.
	 * 
	 * <p>This constructor assumes the required page has already been opened and loaded.
	 *  
	 * @param driver The WebDriver object to use. This should be passed in from the test method.
	 */
	public PageObject(WebDriver driver) {
		this(driver, null);
	}
	
	/**
	 * Instantiates this PageObject and subsequently loads all of the WebElements associated
	 * with this PageObject, annotated with @FindBy and other Selenium PageFactory annotations.
	 * 
	 * <p>If <b>url</b> is provided, that URL will be opened prior to instantiation.
	 *  
	 * @param driver The WebDriver object to use. This should be passed in from the test method.
	 * @param url The URL to load prior to instantiation.
	 */
	public PageObject(WebDriver driver, String url) {
		this.driver = driver;

		if (StringUtils.isEmpty(url)) {
			this.driver.get("http://xpanxion.com/");
		}
		else
		{
			this.driver.get(url);
		}

		this.load();

		// This is gross, but before I was doing a hard wait. Unfortunately, this is also specific to xpanxion.com.
		// Hit _weird_ race conditions in with load times in this vm.
		int lTimeout=30;
        while(lTimeout>0)
        {
			try
			{
				// The header image.
				driver.findElement(By.id("u165"));
				lTimeout=0;
				WaitUtils.hardWait(1000);
			}
			catch(Exception e)
			{
                WaitUtils.hardWait(3000);
                lTimeout-=3;
			}
        }
	}
	
	/**
	 * Initializes the PageFactory annotated WebElements associated with this PageObject instance.
	 * 
	 * This method is called as a consequence of creating a new instance of this PageObject.
	 */
	public void load() {
		PageFactory.initElements(this.driver, this);
	}

	/*
		Click a link on the page based on first partial text match found.
		@param aLinkText The link text to partially match.
		@return The resulting url or null.
	 */
	public String clickLinkByTextGetUrl(String aLinkText)
	{
		// Click a link on the base page by text, and return the resulting url, or null if the linktext is not found.
        // track the old url.
        String lOldUrl = driver.getCurrentUrl();
        driver.findElement(By.partialLinkText(aLinkText)).click();

		return waitForUrlChange(lOldUrl);
	}

	/*
		Click an image on the page based by contents of it's alt text.
		@param aAltText The alt text to match.
		@return The resulting url or null.
	 */
    public String clickImageByAltTextGetUrl(String aAltText)
	{
		String lOldUrl = driver.getCurrentUrl();

		// Click image based on alt text, and anticipate it being a link - return link value.
		//loop through images until we find alt text that matches.
		List<WebElement> lImages = driver.findElements(By.tagName("img"));
		for (int i = 0; i < lImages.size(); i++) {
			if ( lImages.get(i).getAttribute("alt").equals(aAltText))
			{
				lImages.get(i).click();
				break;
			}
		}

		return waitForUrlChange(lOldUrl);
	}

	/*
		Wait for the driver's url to change, and return new url.
		<p>
		This function times out at 30 seconds and returns null
		if the url does not change within that time.
		@param aCurrentUrl The current url to observe change from.
		@return url string or null.
	 */
	private String waitForUrlChange(String aCurrentUrl)
	{
	    String lNewUrl = "";
		int lTimeout=30;
        while(lTimeout>0)
        {
            lNewUrl = driver.getCurrentUrl();
            if(!lNewUrl.equals(aCurrentUrl))
            {
                return lNewUrl;
            }
            WaitUtils.hardWait(3000);
            lTimeout-=3;
        }

        return null;
	}
}
