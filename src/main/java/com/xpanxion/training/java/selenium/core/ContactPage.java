package com.xpanxion.training.java.selenium.core;

import com.gargoylesoftware.htmlunit.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by neeasade on 8/31/15.
 */
public class ContactPage extends PageObject {

    /*
        Instantiate the ContactPage object.
        @param driver The webdriver to use
        @param url the url to load initially.
     */
    public ContactPage(WebDriver driver, String url)
    {
        super(driver,url);
    }

    /*
        Hit the submit button on this contact page.
        <p>
        Assumes that the submit button has class name 'submit-btn'
     */
    public void submit()
    {
        driver.findElement(By.className("submit-btn")).click();
    }

    /*
        Test the value of a web elements CSS property
        NOTE: converts found css value to lower case.
        @param aID The ID of the web element to check.
        @param aCSSProperty the CSS property to verify
        @param aValue The value to test against
        @return True if the element exists and the value is tested correctly, otherwise false.
     */
    public boolean testProperty(String aID, String aCSSProperty, String aValue)
    {
        String lFoundValue = driver.findElement(By.id(aID)).getCssValue(aCSSProperty).toLowerCase();
        return (lFoundValue.equals(aValue));
    }

}
