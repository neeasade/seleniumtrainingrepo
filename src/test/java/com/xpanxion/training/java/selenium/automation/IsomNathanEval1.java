package com.xpanxion.training.java.selenium.automation;

import com.xpanxion.training.java.selenium.core.ContactPage;
import com.xpanxion.training.java.selenium.core.PageObject;
import com.xpanxion.training.java.selenium.core.BaseTest;
import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.swing.text.StyledEditorKit;
import java.util.*;

/*
 * Problem 1 - test the 5 links from xpanxion home page, using the PageObject pattern.
 */
@RunWith(JUnitParamsRunner.class)
public class IsomNathanEval1 extends BaseTest {

    @Test
    public void testHeaderLinks()
    {
        String lBaseUrl = "http://xpanxion.com/";

        // Naming scheme for all tested pages is http://xpanxion.com/<Tested page>.html
        // links to associated pages will be all caps.
        List<String> lLinks = Arrays.asList("about", "approach", "services", "careers", "contact");
        for(Iterator<String> i = lLinks.iterator(); i.hasNext(); ) {
            String lLinkText = i.next();

            // New page object at baseurl - will reset page nav on open driver.
            PageObject lPage = new PageObject(driver,lBaseUrl);

            // Define expected result and click link.
            String aExpectedUrl = lBaseUrl + lLinkText + ".html";
            String lResultUrl = lPage.clickLinkByTextGetUrl(lLinkText.toUpperCase());

            // Test
            Assert.assertEquals(aExpectedUrl,lResultUrl);
        }
    }

    @Test
    @FileParameters("src/main/resources/testinputs.csv")
    public void testFooterLinks(String lLinkText, String lDestinationURL)
    {
        String lBaseUrl = "http://xpanxion.com/";
        PageObject lPage;

        // Make a new page object at base, click link by text, and validate:
        lPage = new PageObject(driver,lBaseUrl);
        Assert.assertEquals(lDestinationURL, lPage.clickImageByAltTextGetUrl(lLinkText));
    }

    @Test
    public void testContactSubmissionFailure()
    {
        final String cTestColor = "rgba(215, 36, 76, 1)";
        final String cTestCSSProperty = "color";
        String[] TestIDs = { "widgetu1680_input", "widgetu1684_input", "widgetu1691_input" };

        // Navigate to contact page to fail submission.
        ContactPage lPage = new ContactPage(driver,"http://xpanxion.com/contact.html");
        lPage.submit();

        // Loop through the web element ID's and test their CSS property.
        for (String lTestID : TestIDs)
        {
            boolean lTestResult = lPage.testProperty(lTestID ,cTestCSSProperty,cTestColor);
            Assert.assertEquals(true,lTestResult);
        }
    }
}
